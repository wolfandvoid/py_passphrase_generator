import random
import os
import time
import subprocess


# Prompt user for an initial string to use for the password / pass phrase
phrase = input('Enter a sentence or phrase you\'d like to use as a password\n')

# Minimum password length, amount of digits, upper/lower and special characters
# This is roughly based on an even distribution of the different types of characters
min_chars = 12
min_digits = 3
min_special = 3
min_upper = 3
min_lower = 3

# % of sentence that are special chars or numbers
max_digits = int(phrase.__len__() / 4)
max_special = int(phrase.__len__() / 4)

# Getting a pseudo random seed
now = (int(str(time.time())[2:4]))
seed = os.urandom(now).__str__()
random.seed = seed

# The chance of some things occurring
chance = 0.3

# A 'l33t speak' character translation table
translation = {
    'i', '1',
    'a', '@',
    'e', '3',
    'o', '0',
    'b', '8',
    's', '$',
    't', '7',
}

chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&'
lower = 'abcdefghijklmnopqrstuvwxyz'
upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
numbers = '0123456789'
special = '!@#$%&'

# Do the work
def generate ():
    phrase = clean_up()
    checks(phrase)
    copy_to_clipboard(phrase)

# Takes random letters and makes them upper case
def capitalise (_phrase):
    _p = list(_phrase)
    for i, c in enumerate(_p):
        if (random.random() < chance):
            _p[i] = c.upper()

    return "".join(_p)

# We don't want any spaces in the password
def remove_spaces (_phrase):
    _p = _phrase.replace(" ", "")
    return _p

# Changes phrase to CamelCase for easier reading and security (adds upper case letters)
def camel_case (_phrase):
    _p = _phrase.title()
    return _p

# L33t speak anyone?
def translate (_phrase):
    pass

# Removes quote characters as they are kinda awkward in passwords
def remove_unwanted_chars (_phrase):
    _p = _phrase.replace("\'", '').replace("\"", '')
    return _p

# Clean up the initial phrase/sentence
def clean_up ():
    new_pass = remove_unwanted_chars(phrase)
    new_pass = camel_case(new_pass)
    new_pass = remove_spaces(new_pass)

    return new_pass

# Runs a number of checks to see if the password is strong enough and has enough variation
def checks (_pass):
    if not is_long_enough(_pass):
        print ('Your phrase is too short')
    elif not has_enough_special_chars(_pass):
        print('Your password doesn\'t have enough special characters')
    elif not has_enough_digits(_pass):
        print('Your password doesn\'t have enough numbers')
    elif not has_enough_upper(_pass):
        print('Your password doesn\'t have enough upper case letters')
    elif not has_enough_lower(_pass):
        print('Your password doesn\'t have enough lower case letters')
    else:
        print ('WHAT AN AMAZING PASSWORD!')


# Check if the password has enough number characters
def has_enough_digits(_phrase):
    num_digits = count_digits(_phrase)

    if (num_digits < min_digits):
        return False
    else:
        return True

# Counts and returns the number of digits in the phrase
def count_digits(_phrase):
    chars = list(_phrase)
    num_digits = 0

    for c in chars:
        if (c.isdigit()):
            num_digits = num_digits + 1

    return num_digits

# Adds _num of digits to the phrase
def add_digits (_phrase, _num):
    str_len = len(_phrase)

    for c in range(_num):
        _phrase = insert_char(_phrase, get_random_char(3), random.randint(0, str_len))

    return _phrase

# Check whether the password has sufficient special characters
def has_enough_special_chars(_phrase):
    num_special = count_spacial_chars (_phrase)

    if (num_special < min_special):
        return [False, num_special]
    else:
        return [True, num_special]

# Count the amount of special characters in the phrase
def count_spacial_chars (_phrase):
    chars = list(_phrase)
    num_special = 0

    for c in chars:
        if  not c.isalnum():
            num_special = num_special + 1

    return num_special

# Add _num of special characters to phrase
def add_special_chars(_phrase, _num):
    str_len = len(_phrase)

    for c in range(_num):
        _phrase = insert_char(_phrase, get_random_char(4), random.randint(0, str_len))

    return _phrase

# Check to see if _phrase is long enough
def is_long_enough (_phrase):
    if (_phrase.__len__() >= min_chars):
        return True

# Check to see if phrase has enough upper case characters
def has_enough_upper (_phrase):
    num_upper = count_upper(_phrase)

    if (num_upper >= min_upper):
        return True
    else:
        return False

# Counts the amount of upper case characters in the phrase
def count_upper (_phrase):
    chars = list(_phrase)
    num_upper = 0

    for c in chars:
        if (c.isupper()):
            num_upper = num_upper + 1

    return num_upper

# Inject _num(ber) of upper case letters to the phrase
def add_upper (_phrase, _num):
    str_len = len(_phrase)

    for c in range(_num):
        _phrase = insert_char(_phrase, get_random_char(2), random.randint(0, str_len))

    return _phrase

# Checks if the password has enough lower case characters
def has_enough_lower(_phrase):
    num_lower = count_lower(_phrase)

    if (num_lower >= min_lower):
        return True
    else:
        return False

# Counts the amount of lower case characters in the phrase
def count_lower (_phrase):
    chars = list(_phrase)
    num_lower = 0

    for c in chars:
        if c.islower():
            num_lower = num_lower + 1

    return num_lower


# Add a _num(ber) of lower case characters to the _phrase
def add_lower(_phrase, _num):
    str_len = len(_phrase)

    for c in range(_num):
        _phrase = insert_char(_phrase, get_random_char(1), random.randint(0, str_len))

    return _phrase

# Adds a _num(ber) of random characters to the phrase
def add_random_chars (_phrase, _num):
    str_len = len(_phrase)

    for c in range(_num):
        _phrase = insert_char(_phrase, get_random_char(0), random.randint(0, str_len))

    return _phrase

# Get a random characters for the passed _type
def get_random_char (_type):
    type = 0

    if (_type == 'any'):
        type = chars
    elif (_type == 'lower'):
        type = lower
    elif (_type == 'upper'):
        type = upper
    elif (_type == 'numbers'):
        type = numbers
    elif (_type == 'special'):
        type = special

    return random.choice(type)

# Inserts a given character at a certain position in the phrase
def insert_char (_phrase, _char, _where):
    tmp = list(_phrase)
    tmp.insert(_where, _char)
    return ''.join(tmp)


# Copies the passed value (password) to the clipboard so user can copy paste it
def copy_to_clipboard(_pass):
    cmd = 'echo ' + _pass.strip() + '|clip'
    return subprocess.check_call(cmd, shell=True)

# Generate the password
# generate()

print(add_random_chars(phrase, 3))